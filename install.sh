#!/usr/bin/env bash
apt-get update
cd ~
#bladeRf
apt-get install -y build-essential libusb-1.0-0-dev libusb-1.0-0 build-essential cmake libncurses5-dev libtecla1 libtecla-dev pkg-config git wget
git clone https://github.com/Nuand/bladeRF.git ./bladeRF
cd ./bladeRF
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DINSTALL_UDEV_RULES=ON ../
make && sudo make install && sudo ldconfig
cd ~
#hackRf
apt-get install -y build-essential cmake libusb-1.0-0-dev pkg-config libfftw3-dev
git clone https://github.com/mossmann/hackrf.git
cd hackrf
mkdir host/build
cd host/build
cmake ..
make && sudo make install && sudo ldconfig
cd ~
#gps-sdr-sim
git clone https://github.com/Nuand/gps-sdr-sim.git
cd gps-sdr-sim
gcc gpssim.c -lm -O3 -o gps-sdr-sim
cp gps-sdr-sim /usr/bin/
cd ~
#misc
apt-get install -y gawk
#nodejs
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - 2>&1
sudo apt-get install -y nodejs
cd /usr/src/payload
npm install
npm build
