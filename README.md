#Build and upload the firmware
##NodeJs
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - 2>&1
sudo apt-get install -y nodejs
```
##config
```
cp ./config/default.json ./config/embedded.json
vim ./config/embedded.json
```
##build and write gpsGram firmware
```
npm run-script  build-embedded
./berryMaker.sh  -e ESSID -p WLAN_PASSWORD -K SSH_PUB_KEY_FILE -s SD_PARTITION
```
`-e`,  `-p`, `-K` are optional.

#Bot commands
`/state` - get application state.

*send geolocation* - start gps signal transmission.

`/stop` - stop signal transmission.

