interface GpsGramConfig {
  telegramToken: string;
  telegramId: string | number;
}
