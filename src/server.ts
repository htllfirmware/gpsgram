import config from 'config';
import Telegram  from './lib/telegram';
import telegramCommandProcessing  from './lib/telegramCommandProcessing';
import GpsGram  from './lib/gpsGram';
const gpsGramConfig = config.get<GpsGramConfig>('gpsGram');
const gpsGram = new GpsGram();
const telegram = new Telegram(gpsGramConfig.telegramToken, gpsGramConfig.telegramId,
    (_telegram: Telegram) => telegramCommandProcessing(_telegram, gpsGram));
const main = (async () => {
  await telegram.sendMessage('gpsGram is online');
  console.log('gpsGram is online');
  try {
    await gpsGram.init({
      feedback:  (msg: string) => telegram.sendMessage(msg)
    });
  } catch (e) {
    await telegram.sendMessage(e.message);
  }
});
main();
