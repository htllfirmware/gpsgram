import util from 'util';
const exec = util.promisify(require('child_process').exec);
const vendorId = [
  [ '1d50' ], // hackRF
  [ '2cf0' ] // bladeRF
];
export default async function detect() {
  for (let i = 0; i < vendorId.length; i++) {
      const detection = await findDevice(vendorId[i]);
      if (detection) {
        return i;
      }
  }
  throw new Error('no compatable device found');
}
async function lsUsb() {
  const {stdout} = await exec(`lsusb  | gawk '{print $6}'`);
  return stdout.split('\n').map( (x: string) => x.split(':')).filter((x: string[]) => x.length === 2);
}
async function findDevice(devices: string[]) {
  const usb = await lsUsb();
  for (const id of devices) {
    if (usb.find((dev: string[]) => dev[0] === id)) {
      return true;
    }
  }
  return false;
}
