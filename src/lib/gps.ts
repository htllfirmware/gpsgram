import fs from 'fs';
import util from 'util';
const exec = util.promisify(require('child_process').exec);
export default async function generateTransmissionPayload(arg: {brdc: string, lat: number, lon: number, hgt: number}) {
    const hgt = arg.hgt || 137;
    const {lat, lon, brdc} = arg;
    try {
      fs.lstatSync('gpssim.bin');
      fs.unlinkSync('gpssim.bin');
    } catch {}
    await exec(`gps-sdr-sim -e ${brdc} -l ${lat},${lon},${hgt}`);
    const list = fs.readdirSync('.');
    const payloadExist = list.find((x: string) => x === 'gpssim.bin');
    return payloadExist !== undefined;
}
