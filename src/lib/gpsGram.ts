import Sdr from './sdr/basic';
import HackRf from './sdr/hackRf';
import BladeRf from './sdr/bladeRf';
import detect from './detectSdr';
import generateTransmissionPayload from './gps';
import * as brdc from './brdc';
export default class GpsGram {
  busy: boolean;
  brdcUpdated: boolean;
  sdrInitialized: boolean;
  initialized: boolean;
  transmitting: boolean;
  brdc: string;
  sdr: Sdr;
  constructor() {
    this.busy = true;
    this.brdcUpdated = false;
    this.sdrInitialized = false;
    this.initialized = false;
    this.transmitting = false;
  }
  async initSdr(arg: {feedback: (msg: string) => Promise<void>}) {
    const sdrType = await detect();
    switch (sdrType) {
      case 0:
        this.sdr = new HackRf();
        await arg.feedback('[+] HackRF found');
      break;
      case 1:
        this.sdr = new BladeRf();
        await arg.feedback('[+] BladeRF found');
      break;
      default:
      throw new Error('Unable to detect SDR');
    }
    this.sdrInitialized = true;
  }
  async updateBrdc(arg: {feedback: (msg: string) => Promise<void>}) {
    const currentBrdcVersion = (brdc.getCurrentBrdc()).version;
    const neededBrdcVersion = await brdc.brdcVersion();
    if (neededBrdcVersion !== currentBrdcVersion) {
      await brdc.clearBrdc();
      await arg.feedback('Downloading new BRDC..');
      await brdc.downloadLastBrdc();
      const downloadedBrdcInfo = brdc.getCurrentBrdc();
      if (neededBrdcVersion !== downloadedBrdcInfo.version) {
        throw new Error('Unable to update BRDC');
      }
      this.brdc = downloadedBrdcInfo.filename;
    } else {
      await arg.feedback('[+] Latest BRDC found');
      this.brdc = brdc.brdcFilename();
    }
    this.brdcUpdated = true;
  }
  async init(arg: {feedback: (msg: string) => Promise<void>}) {
    await arg.feedback('Initializing SDR..');
    await this.initSdr({
      feedback: arg.feedback
    });
    await arg.feedback('Updating BRDC..');
    await this.updateBrdc({
      feedback: arg.feedback
    });
    await arg.feedback('[+] Initialized');
    this.initialized = true;
    this.busy = false;
  }
  async startTransmission(arg: {feedback: (msg: string) => Promise<void>, lat: number, lon: number, hgt?: number}) {
    if (!this.initialized) {
      throw new Error('Device is not initialized');
    }
    if (this.busy) {
      throw new Error('Device is busy');
    }
    this.busy = true;
    await arg.feedback('Generating payload..');
    const generateTransmissionResult = await generateTransmissionPayload({
      brdc: this.brdc,
      lat: arg.lat,
      lon: arg.lon,
      hgt: arg.hgt
    });
    if (!generateTransmissionResult) {
      this.busy = false;
      throw new Error('Unable to generate transmission payload');
    }
    this.transmitting = true;
    await arg.feedback('Sending payload..');
    await this.sdr.transmit();
    this.transmitting = false;
    this.busy = false;
    await arg.feedback('[+] Done');
  }
  async stopTransmission(arg: {feedback: (msg: string) => Promise<void>}) {
    if (!this.initialized) {
      throw new Error('Device is not initialized');
    }
    if (!this.transmitting) {
      throw new Error('Device is not transmitting');
    }
    await this.sdr.stop();
    this.transmitting = false;
    this.busy = false;
    await arg.feedback('[+] Stoped');
  }
}
