import Sdr from './basic';
import util from 'util';
const exec = util.promisify(require('child_process').exec);
export default class BladeRF extends Sdr {
  constructor() {
    super(1);
  }
  async transmit() {
    await exec(`bladeRF-cli -s bladerf.script`);
  }
  async stop() {
    await exec(`killall -9 bladeRF-cli`);
  }
}
