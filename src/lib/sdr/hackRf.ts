import Sdr from './basic';
import util from 'util';
const exec = util.promisify(require('child_process').exec);
export default class HackRF extends Sdr {
  constructor() {
    super(0);
  }
  async transmit() {
    await exec('hackrf_transfer -t gpssim.bin -f 1575420000 -s 2600000 -a 1 -x 0 -R');
  }
  async stop() {
    await exec(`killall -9 hackrf_transfer`);
  }
}
