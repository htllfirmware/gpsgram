import PromiseFtp from 'promise-ftp';
import fs from 'fs';
import util from 'util';
const exec = util.promisify(require('child_process').exec);
const host = 'cddis.gsfc.nasa.gov';
const user = 'ftp';
const password = 'ftp';
const dirPrefix = '/gnss/data/daily';
const dirSuffix = '/brdc';
function getYear() {
    return  (new Date()).getUTCFullYear();
}
function getYearYY() {
    return  (new Date()).getUTCFullYear() % 100;
}
function daysIntoYear() {
    const date = new Date();
    return (Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) - Date.UTC(date.getFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
}
export function brdcVersion() {
    return `${daysIntoYear()}0`;
}
export function brdcFilename() {
    return `brdc${daysIntoYear()}0.${getYearYY()}n`;
}
export function brdcFilenameArchive() {
    return `${brdcFilename()}.Z`;
}
// const brdcArFileRegEx = /^brdc(\d+)\.\d+n\.\Z$/;
const brdcFileRegEx = /^brdc(\d+)\.\d+n$/;
export async function downloadLastBrdc() {
  const year = getYear();
  const ftp = new PromiseFtp();
  await ftp.connect({host, user, password});
  // const list = await ftp.list(`${dirPrefix}/${year}${dirSuffix}`);
  // if (!list || !list.length) {
  //   await ftp.end();
  //   throw new Error('Unable to recieve BRDC version');
  // }
  // const filename = list.filter((x: any) => brdcArFileRegEx.test(x.name))
  //   .sort((x: any, y: any) => y.date.valueOf() - x.date.valueOf())[0].name;
  // if (!filename) {
  //   await ftp.end();
  //   throw new Error('Unable to recieve BRDC version');
  // }
  // const match  = filename.match(brdcArFileRegEx);
  // if (!match || !match[1]) {
  //   await ftp.end();
  //   throw new Error('Unable to recieve BRDC version');
  // }
  // const version = match[1];
  const filename = brdcFilenameArchive();
  const version = brdcVersion();
  const stream = await ftp.get(`${dirPrefix}/${year}${dirSuffix}/${filename}`);
  // console.log(`${dirPrefix}/${year}${dirSuffix}/${filename}`);
  await new Promise(function (resolve, reject) {
      stream.once('close', resolve);
      stream.once('error', reject);
      stream.pipe(fs.createWriteStream(filename));
    });
  await ftp.end();
  await exec(`gunzip -f ${filename}`);
  const extractedFilename = filename.split('.Z')[0];
  return {
    version,
    filename: extractedFilename
  };
}
// export async function getLastBrdcInfo() {
//   const year = getYear();
//   const ftp = new PromiseFtp();
//   await ftp.connect({host, user, password});
//   const list = await ftp.list(`${dirPrefix}/${year}${dirSuffix}`);
//   await ftp.end();
//   if (!list || !list.length) {
//     throw new Error('Unable to recieve BRDC version');
//   }
//   const filename = list.filter((x: any) => brdcArFileRegEx.test(x.name))
//     .sort((x: any, y: any) => y.date.valueOf() - x.date.valueOf())[0].name;
//   if (!filename) {
//     throw new Error('Unable to recieve BRDC version');
//   }
//   const match  = filename.match(brdcArFileRegEx);
//   if (!match || !match[1]) {
//     throw new Error('Unable to recieve BRDC version');
//   }
//   const version = match[1];
//   const extractedFilename = filename.split('.Z')[0];
//   return {
//     version,
//     filename: extractedFilename
//   };
// }
export function getCurrentBrdc() {
  const list = fs.readdirSync('.');
  const filename = list.find((x: string) => brdcFileRegEx.test(x));
  if (!filename) {
    return {
      version: '0',
      filename: ''
    };
  }
  const match  = filename.match(brdcFileRegEx);
  const version = match[1];
  return {version, filename};
}

export function clearBrdc() {
  const list = fs.readdirSync('.');
  const filenames = list.filter((x: string) => brdcFileRegEx.test(x));
  if (!filenames) {
    return true;
  }
  for (const file of filenames) {
       fs.unlinkSync(file);
  }
  return true;
}
