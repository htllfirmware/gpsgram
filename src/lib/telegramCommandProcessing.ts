import Telegram from './telegram';
import TelegramBot from 'node-telegram-bot-api';
import GpsGram from './gpsGram';
async function ping(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  await telegram.bot.sendMessage(chatId, 'yo');
}
async function getState(telegram: Telegram, msg: TelegramBot.Message, gpsGram: GpsGram) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  const state = `initialized: ${gpsGram.initialized}\nbusy: ${gpsGram.busy}\n\
BRDC updated: ${gpsGram.brdcUpdated}\nSDR initialized:${gpsGram.sdrInitialized}\n\
transmitting: ${gpsGram.transmitting}`;
  await telegram.bot.sendMessage(chatId, state);
}
async function stop(telegram: Telegram, msg: TelegramBot.Message, gpsGram: GpsGram) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    await gpsGram.stopTransmission({
      feedback: (message: string) =>  telegram.bot.sendMessage(chatId, message)
    });
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}
async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message, gpsGram: GpsGram) {
    const chatId = msg.chat.id;
    if (chatId !== telegram.telegramId) {
      return;
    }
    console.log(msg);
    if (msg.location) {
      try {
        await gpsGram.startTransmission({
          feedback: (message: string) =>  telegram.bot.sendMessage(chatId, message),
          lat: msg.location.latitude,
          lon: msg.location.longitude
        });
      } catch (e) {
        await telegram.bot.sendMessage(chatId, e.message);
      }
    }
}
export default function telegramCommandProcessing(telegram: Telegram, gpsGram: GpsGram): void {
  telegram.bot.onText(/^\/ping$/, (msg: TelegramBot.Message) => ping(telegram, msg));
  telegram.bot.onText(/^\/state$/, (msg: TelegramBot.Message) => getState(telegram, msg, gpsGram));
  telegram.bot.onText(/^\/stop$/, (msg: TelegramBot.Message) => stop(telegram, msg, gpsGram));
  telegram.bot.on('message', (msg: TelegramBot.Message) => messageProcessing(telegram, msg, gpsGram));
}
